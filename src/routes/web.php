<?php 

// Route::get('/', function () {
//     return view('gen_reports::welcome');
// });

Route::group(['namespace'=>'Esfersoft\Gen_Reports\Controllers'],function(){

	// Route::get('/tables', 'GenReportController@getTables');

	Route::post('/attributes','GenReportController@getAttributes');

	Route::post('/relations','GenReportController@getAllRelations');

	Route::post('/field_type','GenReportController@getFieldType');

	Route::get('/report/{id}','GenReportController@genReport');

	Route::get('/charts/{id}','GenReportController@genReport');

	Route::post('/show-graph','GenReportController@showGraph');

	Route::get('/index','ReportConfigurationController@index');

	Route::get('/create','ReportConfigurationController@create');

	Route::post('/store','ReportConfigurationController@store');

	Route::post('/update/{id}','ReportConfigurationController@update');

	Route::get('/edit/{id}','ReportConfigurationController@edit');
});