<?php

namespace Esfersoft\Gen_Reports\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Esfersoft\Gen_Reports\ReportConfiguration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use ReflectionClass;
use ReflectionMethod;
use ReflectionFunction;


class GenReportController extends Controller
{
	/**
	* @method: getTables
	* @params: 
	* @createddate: 12-02-2019 (dd-mm-yyyy)
	* @developer: Parteek
	* @purpose: To show the table list
	* @return:view
	*/
    // public function getTables(){
    //     try{
    //     	$tables = array_map('reset',DB::select('SHOW TABLES'));
    //     	return view('gen_reports.report_configuration',compact('tables'));
    //     }
    //     catch(Exception $e){
    //         'something went wrong';
    //     }
    // }

    /**
    * @method: get Field types
    * @params: 
    * @createddate: 28-02-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To get the table column field type
    * @return:view
    */
    public function getFieldType(Request $request)
    {
        try{
            $tableName = $request->table;
            $colName = $request->column;
            $field_type = DB::getSchemaBuilder()->getColumnType($tableName, $colName);
            
            return $field_type;
        }
        catch(Exception $e){
            return $e;
        }

    }

    /**
    * @method: getColumns
    * @params: 
    * @createddate: 13-02-2019 (dd-mm-yyyy)
    * @developer: Parteek
    * @purpose: To get the columns list based on table
    * @return:view
    */
    public function getAttributes(Request $request){
        $model = new $request->data;
        $table = $model->getTable();
        if(!empty($table)){
                $column = Schema::getColumnListing($table);
            if(!empty($column)){
                array_push($column,$table);
                return $column;
            }
            else{
                return response()->json(['error_msg'=>'There is no table in the database'],404);
            }
        }
    }

    public function showGraph(Request $request){

        $labels = $request->labels;
        $datasets = array_values($request->data);
        $graphType = $request->graphType;
        // foreach ($labels as $value) {
        //    if(array_key_exists($value,$data)){
        //         $datasets[] = array_sum($data[$value]);
        //    }
        // }
        // dd($datasets);
        return response()->json(['datasets'=> $datasets,'graphId'=> $graphType,'labels' => $labels]);
    }

    public function genReport($id){

        try{
            $configuration = ReportConfiguration::findOrFail($id);
            // dd($configuration);
            $allData = json_decode($configuration->configuration);
            // dd($allData);
            // $relation1 = $allData->model_relation1; // relaion between models
            if($allData->base_model){
                $baseModel = new $allData->base_model;  //base model
                $baseTable = $baseModel->getTable(); // base table
                $baseModelKey = $baseModel->getKeyName(); //base model primary key
                $baseModelColumns = $allData->$baseTable; // base model columns
                $columns = $baseModelColumns;
            }
            // if($allData->$baseTable){
            // }
            if(!empty($allData->related_model1)){
                $relatedModel1 = new $allData->related_model1; // relational model
                $relatedTable1 = $relatedModel1->getTable(); // relational table
                $relatedModel1ForeignKey = $relatedModel1->getForeignKey(); // related model self foreign key
                if(!empty($allData->$relatedTable1)){
                    $relatedModel1Columns = $allData->$relatedTable1;
                    $columns = array_merge($baseModelColumns,$relatedModel1Columns);
                }
            }
            if(!empty($allData->related_model2)){
                $relatedModel2 = new $allData->related_model2; // relational model
                $relatedTable2 = $relatedModel2->getTable(); // relational table
                $relatedModel2ForeignKey = $relatedModel2->getForeignKey(); //related model self foreign key
                if(!empty($allData->$relatedTable2)){
                    $relatedModel2Columns = $allData->$relatedTable2;
                    $columns = array_merge($baseModelColumns,$relatedModel1Columns,$relatedModel2Columns);
                }
            }
            if(!empty($allData->related_model3)){
                $relatedModel3 = new $allData->related_model3; // relational model
                $relatedTable3 = $relatedModel3->getTable(); // relational table
                $relatedModel3ForeignKey = $relatedModel3->getForeignKey(); //related model self foreign key
                if(!empty($allData->$relatedTable3)){
                    $relatedModel3Columns = $allData->$relatedTable3;
                    $columns = array_merge($baseModelColumns,$relatedModel1Columns,$relatedModel2Columns,$relatedModel3Columns);
                }
            }
            //Multiple Computed Fields
            if(in_array(null,$allData->computed_fields,true)){

            }
            else{
                $computeData = $allData->computed_fields;
                // dd($computeData);
                if(count($computeData)>4){
                   $computeData = array_chunk($computeData,4);
                   foreach ($computeData as $value) {
                       $computeFields[] = DB::raw($value[1].'('.$value[0].') '.$value[2].' '.$value[3]);
                   }
                }
                else{
                    $computeFields[] = DB::raw($computeData[1].'('.$computeData[0].') '.$computeData[2].' '.$computeData[3]);
                }
                $columns = array_merge($columns,$computeFields);
                // dd($columns);
            }
            $baseModelForeignKey = $baseModel->getForeignKey(); // base model self foreign key
            //Group by clause
            $groupBy = '';
            if(!empty($allData->groupby)){
                $groupBy = $allData->groupby;
                // $groupBy = implode("','",$allData->groupby);
                    // dd($groupBy);
            }
            // dd($columns);

            //Multiple Where Condition
            if(!empty($allData->where_condition)){
                $whereData = $allData->where_condition;
                if(count($whereData)>3){
                    $whereData = array_chunk($allData->where_condition,3);
                    // dd($whereData);
                }
                 else{
                    $whereData = [$allData->where_condition];
                    // dd($whereData);
                }
            }

            if(!empty($allData->base_model) and !empty($allData->related_model1) and !empty($allData->related_model2) and !empty($allData->related_model3)){
                $data = DB::table($baseTable)
                        ->join($relatedTable1,$relatedTable1.'.'.$baseModelForeignKey,'=',$baseTable.'.'.'id')
                        ->join($relatedTable2,$relatedTable2.'.'.$relatedModel1ForeignKey,'=',$relatedTable1.'.'.'id')
                        ->join($relatedTable3,$relatedTable3.'.'.$relatedModel2ForeignKey,'=',$relatedTable2.'.'.'id')
                        ->where($whereData)
                        ->select($columns)
                        // ->orderBy($relatedTable3.'.'.'created_at','desc')
                        ->groupBy($groupBy)
                        ->get()->toArray();
            }

            elseif (!empty($allData->base_model) and !empty($allData->related_model1) and !empty($allData->related_model2)) {
                
                $data = DB::table($baseTable)
                        ->join($relatedTable1,$relatedTable1.'.'.$baseModelForeignKey,'=',$baseTable.'.'.'id')
                        ->join($relatedTable2,$relatedTable2.'.'.$relatedModel1ForeignKey,'=',$relatedTable1.'.'.'id')
                        ->where($whereData)
                        ->select($columns)
                        // ->orderBy($relatedTable2.'.'.'created_at','desc')
                        ->groupBy($groupBy)
                        ->get()->toArray();
            }
            elseif (!empty($allData->base_model) and !empty($allData->related_model1)) {
                $data = DB::table($baseTable)
                        ->join($relatedTable1,$relatedTable1.'.'.$baseModelForeignKey,'=',$baseTable.'.'.'id')
                        ->select($columns)
                        ->where($whereData)
                        // ->orderBy($relatedTable1.'.'.'created_at','desc')
                        ->groupBy($groupBy)
                        ->get()->toArray();
            }
            elseif (!empty($allData->base_model)) {
                $data = DB::table($baseTable)
                        ->where($whereData)
                        ->select($columns)
                        // ->orderBy($baseTable.'.'.'created_at','desc')
                        ->groupBy($groupBy)
                        ->get()->toArray();
            }
            // dd($data);
            if(\Route::current()->uri()==="report/{id}"){
                return view('gen_reports::report',compact('data','columns'));
            }
            elseif (\Route::current()->uri()==="charts/{id}") {
                return view('gen_reports::graph',compact('data'));
            }
        }
        catch(Exception $e){
            return $e;
        }
    }

 	/**
 	* @method: getColumns
 	* @params: 
 	* @createddate: 24-02-2019 (dd-mm-yyyy)
 	* @developer: Parteek
 	* @purpose: To get the relationships of particular model
 	* @return:relationships
 	*/
    public function getAllRelations(Request $request,\Illuminate\Database\Eloquent\Model $model = null)
    {
        try {
            $heritage = 'all';
            $modelName = $request->modelName;
            $model = new $modelName;
            $types = ['children' => 'Has', 'parents' => 'Belongs', 'all' => ''];
            $heritage = in_array($heritage, array_keys($types)) ? $heritage : 'all';
            // if (\Illuminate\Support\Facades\Cache::has($modelName."_{$heritage}_relations")) {
            //     die('ok');
            //     return \Illuminate\Support\Facades\Cache::get($modelName."_{$heritage}_relations"); 
            // }

            $reflectionClass = new ReflectionClass($model);
            $traits = $reflectionClass->getTraits();    // Use this to omit trait methods
            $traitMethodNames = [];
            foreach ($traits as $name => $trait) {
                $traitMethods = $trait->getMethods();
                foreach ($traitMethods as $traitMethod) {
                    $traitMethodNames[] = $traitMethod->getName();
                }
            }
            // Checking the return value actually requires executing the method.  So use this to avoid infinite recursion.
            $currentMethod = collect(explode('::', __METHOD__))->last();
            $filter = $types[$heritage];
            $methods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);  // The method must be public
            $methods = collect($methods)->filter(function ($method) use ($modelName, $traitMethodNames, $currentMethod) {
                $methodName = $method->getName();
                if (!in_array($methodName, $traitMethodNames)   //The method must not originate in a trait
                    && strpos($methodName, '__') !== 0  //It must not be a magic method
                    && $method->class === $modelName    //It must be in the self scope and not inherited
                    && !$method->isStatic() //It must be in the this scope and not static
                    && $methodName != $currentMethod    //It must not be an override of this one
                ) {
                    $parameters = (new \ReflectionMethod($modelName, $methodName))->getParameters();
                    return collect($parameters)->filter(function ($parameter) {
                        return !$parameter->isOptional();   // The method must have no required parameters
                    })->isEmpty();  // If required parameters exist, this will be false and omit this method
                }
                return false;
            })->mapWithKeys(function ($method) use ($model, $filter) {
                $methodName = $method->getName();
                $relation = $model->$methodName();  //Must return a Relation child. This is why we only want to do this once
                if (is_subclass_of($relation, \Illuminate\Database\Eloquent\Relations\Relation::class)) {
                    $type = (new \ReflectionClass($relation))->getShortName();  //If relation is of the desired heritage
                    if (!$filter || strpos($type, $filter) === 0) {
                        return [$methodName => get_class($relation->getRelated())]; // ['relationName'=>'relatedModelClass']
                    }
                }
                return false;   // Remove elements reflecting methods that do not have the desired return type
            })->toArray();

            // \Illuminate\Support\Facades\Cache::forever($modelName."_{$heritage}_relations", $methods);
            if($methods){
                return response()->json($methods);
            }
            else {
                return response()->json(['error_msg'=>'there is no relation in the model'],404);
            }
        }  catch(Exception $e){
            return $e;
        }
    }
}
