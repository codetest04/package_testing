<?php

namespace Esfersoft\Gen_Reports\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Container\Container;
use ReflectionClass;
use ReflectionMethod;
use ReflectionFunction;
use Illuminate\Database\Eloquent\Model;
use Esfersoft\Gen_Reports\ReportConfiguration;

class ReportConfigurationController extends Controller
{
    public function index(){
    	try{
    		$configurations = ReportConfiguration::all();
    		return view('gen_reports::configuration_index',compact('configurations'));
    	}
    	catch(Exception $e){
    	    return $e;
    	}
    }

    public function create(){
            $models = collect(File::allFiles(app_path()))
        ->map(function ($item) {
            $path = $item->getRelativePathName();
            $class = sprintf('\%s%s',
                Container::getInstance()->getNamespace(),
                strtr(substr($path, 0, strrpos($path, '.')), '/', '\\'));

            return $class;
        })
        ->filter(function ($class) {
            $valid = false;

            if (class_exists($class)) {
                $reflection = new ReflectionClass($class);
                $valid = $reflection->isSubclassOf(Model::class) &&
                    !$reflection->isAbstract();
            }

            return $valid;
        });

        foreach( $models->values() as $value){
            $value = ltrim($value,$value[0]);
            $model_instance = new $value;
            $tables[$value] = $model_instance->getTable(); 
        }
        return view('gen_reports::report_configuration')->with('tables',$tables);
    }

    public function store(Request $request){
        // dd($request->all());
        $data = $request->except('_token');
        $configuration = new ReportConfiguration;
        $configuration->name = $request->report_name;
        $configuration->description = $request->report_description;
        $configuration->configuration = json_encode($data);
        $data = $configuration->save();
        if($data){
            return redirect('/index')->with('success','Configuration added successfully');
        }
        else{
            return back()->with('error','Configuration occur some error');
        }
    }

    public function edit($id){
            $models = collect(File::allFiles(app_path()))
        ->map(function ($item) {
            $path = $item->getRelativePathName();
            $class = sprintf('\%s%s',
                Container::getInstance()->getNamespace(),
                strtr(substr($path, 0, strrpos($path, '.')), '/', '\\'));

            return $class;
        })
        ->filter(function ($class) {
            $valid = false;

            if (class_exists($class)) {
                $reflection = new ReflectionClass($class);
                $valid = $reflection->isSubclassOf(Model::class) &&
                    !$reflection->isAbstract();
            }

            return $valid;
        });

        foreach( $models->values() as $value){
            $value = ltrim($value,$value[0]);
            $model_instance = new $value;
            $tables[$value] = $model_instance->getTable(); 
        }
    	try{
    		$configuration = ReportConfiguration::findOrFail($id);

    		$data = json_decode($configuration->configuration);
    		return view('gen_reports::edit_report_configuration',compact('data','tables','id'));
    	}
    	catch(Exception $e){
    	    return $e;
    	}
    }

    public function update(Request $request,$id){
        try{
            $configuration = ReportConfiguration::findOrFail($id);
            $data = $request->except('_token');
            $configuration->name = $request->report_name;
            $configuration->description = $request->report_description;
            $configuration->configuration = json_encode($data);
            $data = $configuration->save();
            if($data){
                return redirect('/index')->with('success','Configuration updated successfully');
            }
            else{
                return back()->with('error','Configuration occur error while updating');
            }
        }
        catch(Exception $e){
            return $e;
        }
    }
}
