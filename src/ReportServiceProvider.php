<?php

namespace Esfersoft\Gen_Reports;

use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/views', 'gen_reports');
         $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->publishes([
            __DIR__.'/views' => resource_path('views/gen_reports')
        ],'reportviews');
        $this->publishes([
            __DIR__.'/public' => public_path('gen_reports'),
        ], 'public');
    }
}
