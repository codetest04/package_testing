<?php 

	$base_model = '';
	$base_table = '';
	$related_model1 = '';
	$related_table1 = '';
	$related_model2 = '';
	$related_table2 = '';
	$related_model3 = '';
	$related_table3 = '';

	if($data->base_model){
		$base_model = new $data->base_model;
		$base_table =  $base_model->getTable();
	}
	if($data->related_model1){
		$related_model1 = new $data->related_model1;
		$related_table1 = $related_model1->getTable();
	}
	if($data->related_model2){
		$related_model2 = new $data->related_model2;
		$related_table2 = $related_model2->getTable();
	}
	if($data->related_model3){
		$related_model3 = new $data->related_model3;
		$related_table3 = $related_model3->getTable();
	}
	function operators($val){
		if($val==="="){
			return "Equal";
		}
		elseif($val==="!="){
			return 'Not Equal';
		}
		elseif($val===">"){
			return 'Greater Than';
		}
		elseif($val==="<"){
			return 'Less than';
		}
		elseif($val===">=") {
			return 'Greter than Equal to';
		}
		elseif($val==="<="){
			return 'Less than Equal to';
		}
		elseif ($val==="between") {
			return 'between';
		}
		else {
			return 'operator not found';
		}
	}
	//operators
	// if()
	// $operators = ['=','!=','>','<','>=','<=','between'];

?>
@extends('gen_reports::layouts.app')

@section('header')
<h1>Edit Report Configuration</h1>
@endsection

@section('content')
<form role="form" action="{{ url('/update',['id'=>$id]) }}" method="post" enctype="multipart/form-data">
	@csrf
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Edit Report Configuration </h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" name="report_name" id="report_name" value="{{ $data->report_name }}" required>
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" rows="3" placeholder="Enter ..." name="report_description" id="report_description" required>{{ $data->report_description }}</textarea>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Model and Attributes</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Base Model</label>
						<select class="form-control" id="base_model" name="base_model" style="width: 100%;">
							<option value="">Select Base Model</option>
							@foreach($tables as $model => $table)
							<?php
							$selected="";
							if($data->base_model === $model){
							  $selected="selected";
							}
							?> 
							   <option value="{{ $model }}" {{ $selected }}>{{ $table }}</option>
							@endforeach							
						</select>
					</div>
					<div class="form-group">
						<label>Related Model1</label>
						<select class="form-control" id="related_model1" name="related_model1" style="width: 100%;">
							<option value="{{ $data->related_model1 ? $data->related_model1:' ' }}" selected>{{ $related_table1 }}</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Related Model2</label>
						<select class="form-control" id="related_model2" name="related_model2" style="width: 100%;">
							<option value="{{ $data->related_model2 ? $data->related_model2:' ' }}" selected>{{ $related_table2 }}</option>
						</select>
					</div>
					<div class="form-group">
						<label>Related Model3</label>
						<select class="form-control" id="related_model3" name="related_model3" style="width: 100%;">
							<option value="{{ $data->related_model3 ? $data->related_model3: ' ' }}" selected>{{ $related_table3 }}</option>
						</select>
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					@if(isset($data->$base_table))
						<label>{{ $base_table }}</label>
					@endif
					<div class="form-group" id="model_attributes">
						@if(isset($data->$base_table))
							@foreach($data->$base_table as $value)
							<div class="form-check d-inline">
								<input class="form-check-input" type="checkbox" name="{{ $base_table.'[]' }}" data-table="{{ $base_table }}" value="{{ $value }}" checked>
								<label for="id" class="form-check-label">{{ $value }}</label>
							</div>
							@endforeach
						@endif
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					@if(isset($data->$related_table1))
						<label>{{ $related_table1 }}</label>
					@endif
					<div class="form-group" id="related_model1_attributes">
						@if(isset($data->$related_table1))
							@foreach($data->$related_table1 as $value)
							<div class="form-check d-inline">
								<input class="form-check-input" type="checkbox" name="{{ $related_table1.'[]' }}" data-table="{{ $related_table1 }}" value="{{ $value }}" checked>
								<label for="id" class="form-check-label">{{ $value }}</label>
							</div>
							@endforeach
						@endif
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					<!-- checkbox -->
					@if(isset($data->$related_table2))
					<label>{{ $related_table2 }}</label>
					@endif
					<div class="form-group" id="related_model2_attributes">
						@if(isset($data->$related_table2))
						@foreach($data->$related_table2 as $value)
						<div class="form-check d-inline">
							<input class="form-check-input" type="checkbox" name="{{ $related_table2.'[]' }}" data-table="{{ $related_table2 }}" value="{{ $value }}" checked>
							<label for="id" class="form-check-label">{{ $value }}</label>
						</div>
						@endforeach
						@endif
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					<!-- checkbox -->
					@if(isset($data->$related_table3))
					<label>Releted Model3</label>
					@endif
					<div class="form-group" id="related_model3_attributes">
						@if(isset($data->$related_table3))
							@foreach($data->$related_table3 as $value)
							<div class="form-check d-inline">
								<input class="form-check-input" type="checkbox" name="{{ $related_table3.'[]' }}" data-table="{{ $related_table3 }}" value="{{ $value }}" checked>
								<label for="id" class="form-check-label">{{ $value }}</label>
							</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Conditions</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool various_conditions" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body" style="display: block;">
			@if($data->where_condition)
			@if(count($data->where_condition)>3)
			<?php 
				$wh_condition = array_chunk($data->where_condition,3);
			?>
			@foreach($wh_condition as $whereData)
			@if($loop->first)
			<div class="row" id="wh_condition">
				<div class="form-group col-4 mt-3">
					<label>Base Model Columns</label>
					<select class="form-control field_type" name="where_condition[]" id="where_condition" style="width: 100%;">
						<option value="{{ $whereData[0] }}" selected>{{ $whereData[0] }}</option>
					</select>
				</div>
				<div class="form-group col-4 mt-3">
					<label>Operations</label>
					<select class="form-control operators" name="where_condition[]" style="width: 100%;" id="operators">
						<option value="{{ $whereData[1] }}" selected>{{ operators($whereData[1]) }}</option>
					</select>
				</div>
				<div class="form-group col-4 mt-3">
					<label>Condition Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="where_condition[]" value="{{ $whereData[2] }}" id="wh_condition_input">
						<span class="input-group-append plus-inner">
							<button type="button" class="btn btn-info btn-flat add_multi_where">+</button>
						</span>                           
					</div>
				</div>
			</div>
			@else
			<div class="row multi_inputs">
				<div class="form-group col-4 mt-3">
					<label>Base Model Columns</label>
					<select class="form-control base_columns field_type" name="where_condition[]" style="width: 100%;">
						<option value="{{ $whereData[0] }}" selected>{{ $whereData[0] }}</option>
					</select>
				</div>
				<div class="form-group col-4 mt-3">
					<label>Operations</label>
					<select class="form-control operators" name="where_condition[]" style="width: 100%;">
						<option value="{{ $whereData[1] }}" selected>{{ operators($whereData[1]) }}</option>
					</select>
				</div>
				<div class="form-group col-4 mt-3">
					<label>Condition Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="where_condition[]" value="{{ $whereData[2] }}">
						<span class="input-group-append plus-inner">
							<button type="button" class="btn btn-danger btn-flat remove_button">-</button>
						</span>
					</div>
				</div>
			</div>
			@endif
			@endforeach
			@else
			<?php  $whereData = $data->where_condition; ?>
				<div class="row" id="wh_condition">
					<div class="form-group col-4 mt-3">
						<label>Base Model Columns</label>
						<select class="form-control field_type" name="where_condition[]" id="where_condition" style="width: 100%;">
							<option value="{{ $whereData[0] }}" selected>{{ $whereData[0] }}</option>
						</select>
					</div>
					<div class="form-group col-4 mt-3">
						<label>Operations</label>
						<select class="form-control operators" name="where_condition[]" style="width: 100%;" id="operators"s>
							<option value="{{ $whereData[1] }}" selected>{{ operators($whereData[1]) }}</option>
						</select>
					</div>

					<div class="form-group col-4 mt-3">
						<label>Condition Value</label>
						<div class="plus-wrap">
							<input type="text" class="form-control" name="where_condition[]" value="{{ $whereData[2] }}" id="wh_condition_input">
							<span class="input-group-append plus-inner">
								<button type="button" class="btn btn-info btn-flat add_multi_where">+</button>
							</span>                           
						</div>
					</div>
				</div>
			@endif
			@endif
			
			<div class="row">
				<div class="col-12 mt-3">
					<label>Group BY</label>
					<select class="form-control select2" id="groupby_columns" name="groupby[]" style="width: 100%;" data-placeholder="select a column" multiple>
						@if(isset($data->groupby))
						@foreach($data->groupby as $value)
							<option value="{{ $value }}" selected>{{ $value }}</option>
						@endforeach
						@endif
					</select>
				</div>
			</div>
			
			@if($data->computed_fields)
			@if(count($data->computed_fields)>4)
			<?php 
				$computed_fields = array_chunk($data->computed_fields,4);
			?>
			@foreach($computed_fields as $computeData)
			@if($loop->first)
			<div class="row" id="compute_module">
				<div class="form-group col-3 mt-3">
					<label>Selected Columns</label>
					<select class="form-control get_computed_fields" name="computed_fields[]" id="compute_selected_columns" style="width: 100%;">
						<option value="{{ $computeData[0] }}" selected>{{ $computeData[0] }}</option>
					</select>
				</div>
				<div class="form-group col-3 mt-3">
					<label>Computed Functions</label>
					<select class="form-control" name="computed_fields[]" id="computed_fields" style="width: 100%;">
						<option value="{{ $computeData[1] }}" selected>{{ $computeData[1] }}</option>
					</select>
				</div>

				<div class="form-group col-3 mt-3">
					<label>Conditional Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="As" readonly>                   
					</div>
				</div>
				<div class="form-group col-3 mt-3">
					<label>New Column Name </label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="{{ $computeData[3] }}" id="compute_input2">
						<span class="input-group-append plus-inner">
							<button type="button" class="btn btn-info btn-flat add_multi_computed">+</button>
						</span>      
					</div>
				</div>
			</div>
			@else
			<div class="row multi_inputs">
				<div class="form-group col-3 mt-3">
					<label>Selected Columns</label>
					<select class="form-control base_computed_columns get_computed_fields" name="computed_fields[]" style="width: 100%;">
						<option value="{{ $computeData[0] }}" selected>{{ $computeData[0] }}</option>
					</select>
				</div>
				<div class="form-group col-3 mt-3">
					<label>Computed Functions</label>
					<select class="form-control" name="computed_fields[]" id="computed_fields" style="width: 100%;">
						<option value="{{ $computeData[1] }}" selected>{{ $computeData[1] }}</option>
					</select>
				</div>
				<div class="form-group col-3 mt-3">
					<label>Conditional Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="As" readonly></div>
					</div>
				<div class="form-group col-3 mt-3">
					<label>New Column Name </label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="{{ $computeData[3] }}"><span class="input-group-append plus-inner"><button type="button" class="btn btn-danger btn-flat remove_button">-</button></span>
					</div>
				</div>
			</div>
			@endif
			@endforeach
			@else
			<?php  $computeData = $data->computed_fields; ?>
			<div class="row" id="compute_module">
				<div class="form-group col-3 mt-3">
					<label>Selected Columns</label>
					<select class="form-control get_computed_fields" name="computed_fields[]" id="compute_selected_columns" style="width: 100%;">
						<option value="{{ $computeData[0] }}" selected>{{ $computeData[0] }}</option>
					</select>
				</div>
				<div class="form-group col-3 mt-3">
					<label>Computed Functions</label>
					<select class="form-control" name="computed_fields[]" id="computed_fields" style="width: 100%;">
						<option value="{{ $computeData[1] }}" selected>{{ $computeData[1] }}</option>
					</select>
				</div>

				<div class="form-group col-3 mt-3">
					<label>Conditional Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="As" readonly>                   
					</div>
				</div>
				<div class="form-group col-3 mt-3">
					<label>New Column Name </label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="{{ $computeData[3] ? $computeData[3]:''}}" id="compute_input2">
						<span class="input-group-append plus-inner">
							<button type="button" class="btn btn-info btn-flat add_multi_computed">+</button>
						</span>      
					</div>
				</div>
			</div>
			@endif
			@endif
                    
				<!-- /.col -->

				<!-- /.col -->
			<!-- </div> -->
			<!-- /.row -->
		</div>
		<!-- /.card-body -->
		<div class="card-footer">
			<!-- <input type="submit" class="btn btn-primary" name="preview" value="Preview"> -->
			<input type="submit" class="btn btn-primary" name="save_configuration" value="Save Configuration">
		</div>  
	</div>
</form>
<!-- <script type="text/javascript">
	$(function() {	
     //this calls it on load
     var related_model1 = "{{ $data->related_model1 }}";
     console.log(related_model1);
    	// var check1 = $("#base_model").trigger("change");
    	var relatedmodel1 = $('#relatedmodel1').val();

    	// $('#related_model1').trigger("change");
	});
</script> -->
@endsection