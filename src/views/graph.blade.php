@extends('gen_reports::layouts.app')

@section('content')
<?php 
  // echo "<pre>";
  // print_r($data);
  // die('ok');
    $i = 1;
  foreach($data as $graphData){
    foreach ($graphData as $key => $value) {
      $filter = gettype($value);
      if($filter==="integer" || $filter==="double"){
        // die('ok');
      $datasets[$key] = $i;
      if($i>1){
        continue;
      }
        $labels[] = $key;
      }
     
    }
    $i++;
  }
?>	
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Graph</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" id="show_graph">
    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Columns in graph</label>
        <select class="form-control select2" name="labels[]" id="labels" style="width: 100%;"data-placeholder='Select Columns for graph' required multiple>
          @if(isset($labels))
          @foreach($labels as $label)
            <option value="{{ $label }}">{{ $label }}</option>
          @endforeach
          @endif
        </select>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Graph Type</label>
        <select class="form-control" name="graph_type" id="graph_type" style="width: 100%;" required>
          <option value="" selected>Select Graph Type</option>
<!--             <option value="barChart">Bar Chart</option>
            <option value="lineChart">Line Chart</option> -->
            <option value="pieChart">Pie Chart</option>
        </select>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Show Graph</button>
    </div>
  </form>
</div>
<div class="card card-primary" style="display:none;" id="open_graph">
  <div class="card-header">
    <h3 class="card-title">Area Chart</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
    </div>
  </div>
  <div class="card-body">
    <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
    <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px;max-width: 100%; display: block;" width="455" height="250" class="chartjs-render-monitor change_chart"></canvas>
    </div>
  </div>
<!-- /.card-body -->
</div>
<script type="text/javascript">
  $('#show_graph').submit(function(e){
    e.preventDefault();

    var labels = $("#labels").val();
    var labelLength = labels.length;
    var graphType = $('#graph_type').val();
    var data = <?php if(isset($datasets)){echo json_encode($datasets);} ?>;

    var colors = ['#CD5C5C','#F08080','#FA8072','#E9967A','#DC143C','#FF0000','#FF7F50','#FFA500','#FF8C00','#FFD700','#FFE4B5','#BDB76B','#DDA0DD','#DA70D6','#9370DB','#8B008B','#6A5ACD','#7B68EE','#32CD32','#2E8B57','#6B8E23','#008B8B','#48D1CC']
    // var randomColor = colors[Math.floor(Math.random() * colors.length)];
    // console.log(randomColor);
    // var graphData = $.map(data,function(element){
    //   console.log(element);
    // });
    // $.each(data,function(key,value){
    //   if(key===labels[i])
    //   i++;
    // });
    // console.log(data);
    function getRandom(arr, n) {
        var result = new Array(n),
            len = arr.length,
            taken = new Array(len);
        if (n > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (n--) {
            var x = Math.floor(Math.random() * len);
            result[n] = arr[x in taken ? taken[x] : x];
            taken[x] = --len in taken ? taken[len] : len;
        }
        return result;
    }

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/show-graph',
      type:'POST',
      data:{
        data:data,
        labels:labels,
        graphType:graphType
      },
      success:function(data) {
        console.log(data);
        $('#open_graph').show();
        $('.change_chart').attr('id',data.graphId);
        var graphId = data.graphId;
        $('h3:eq(1)').replaceWith(graphId);
        // var graphdata = [$.each(data.datasets,function(key,value){
                          
        //                     Math.round(value);
        //                 })];
        console.log(data.datasets);
        var graphData  = {
          labels: data.labels,
          datasets: [
                {
                  data: data.datasets,
                  backgroundColor : getRandom(colors,labelLength)
                }
          ] 
        }

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
        var pieData        = graphData;
        var pieOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var pieChart = new Chart(pieChartCanvas, {
          type: 'pie',
          data: pieData,
          options: pieOptions      
        })

      },
      error:function(data){
       alert('somethng went wrong');
      },
    });
  });
</script>
<!-- <script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //-------------
    //- LINE CHART -
    //--------------
    // var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
    // var lineChartOptions = jQuery.extend(true, {}, areaChartOptions)
    // var lineChartData = jQuery.extend(true, {}, areaChartData)
    // lineChartData.datasets[0].fill = false;
    // lineChartData.datasets[1].fill = false;
    // lineChartOptions.datasetFill = false

    // var lineChart = new Chart(lineChartCanvas, { 
    //   type: 'line',
    //   data: lineChartData, 
    //   options: lineChartOptions
    // })


    var donutData        = {
      labels: [
          'Chrome', 
          'IE',
          'FireFox', 
          'Safari', 
          'Opera', 
          'Navigator', 
      ],
      datasets: [
        {
          data: [700,500,400,600,300,100],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions      
    })

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar', 
      data: barChartData,
      options: barChartOptions
    })
  })
</script> -->
@endsection