
@extends('gen_reports::layouts.app')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Report  Preview</h3>

          <div class="card-tools">
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="height: 300px;">
          <table class="table table-head-fixed text-nowrap" id="tblReport">
            <thead>
              @if($data)
              <tr>
                <th>S.NO.</th>
                @foreach($data as $head)
                  @if($loop->first)
                    @foreach($head as $key=>$value)
                      <th>{{ $key }}</th>
                    @endforeach
                  @endif
                @endforeach
              </tr>
              @endif
            </thead>
            <tbody>
              @if($data)
              @foreach($data as $table)
                <?php $total_row = $loop->count;?>
                <tr>
                  <td>{{ $loop->iteration }}</td>
                @foreach($table as $key => $val)
                    <td>{{ \Illuminate\Support\Str::limit($val,20,'....') }}</td>
                @endforeach 
                </tr>                
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <div class="card-footer">
<!--         <div class="btn-group">
            <button type="button" class="btn btn-info">Action</button>
            <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
              <span class="sr-only">Toggle Dropdown</span>
              <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-1px, 37px, 0px);">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Separated link</a>
              </div>
            </button>
          </div> -->
          <button type="button" class="btn btn-primary" id="gen_excel">Generate Excel</button>
          <button type="button" class="btn btn-primary" id="gen_csv">Generate Csv</button>
          <button type="button" class="btn btn-primary" id="gen_pdf">Generate Pdf</button>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@endsection