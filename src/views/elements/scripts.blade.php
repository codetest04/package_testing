<!-- <script type="text/javascript">
    // select2 place holder text
    $('.sel_mul').select2({
    placeholder: 'Choose multiple Table'
    });
</script> -->
<!-- jQuery -->

<!-- Bootstrap 4 -->
<!-- Select2 -->
<script src="{{ asset('gen_reports/plugins/select2/js/select2.full.min.js') }}"></script>

<script src="{{ asset('gen_reports/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('gen_reports/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('gen_reports/plugins/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('gen_reports/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('gen_reports/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

<script src="{{ asset('gen_reports/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('gen_reports/plugins/chart.js/Chart.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('gen_reports/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('gen_reports/dist/js/demo.js') }}"></script>

<!-- <script src="{{ asset('gen_reports/plugins/chart.js/Chart.bundle.min.js') }}"></script> -->

<script src="{{ asset('gen_reports/dist/js/table2csv.min.js') }}"></script>

<script src="{{ asset('gen_reports/plugins/pdfmake/pdfmake.min.js') }}"></script>

<script src="{{ asset('gen_reports/plugins/pdfmake/vfs_fonts.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<script src="{{ asset('gen_reports/dist/js/jquery.table2excel.js') }}"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

  })
</script>
  <script>
      $(document).ready(function(){
        $('.alert-success,.alert-danger').fadeOut(4000);
      });
</script>
<script>
  $('#base_model').on('change',function(){
    var modelName = $(this).val();
    $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/relations',
      type:'POST',
      data:{
        modelName
      },
      success:function(data){

        $('#related_model1,#related_model2,#related_model3').empty().append('<option value="" selected>Select Relation</option>');
        $(":input[name='model_relation1']").remove();
        $.each(data,function(key,value){
          console.log('check');
          $('#related_model1').after('<input type="hidden" class="model_relation" name="model_relation1"'+'value="'+key+'"'+'>');
            $('#related_model1').append('<option value='+value+'>'+key+'</option>');
        });

        if(data){
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/attributes',
            type:'POST',
            data:{
              data:modelName
            },
            success:function(data){

              var table_name = data[data.length-1]; 
              var attributes = data.pop();
              $('#model_attributes').before('<label>'+table_name+'</label>');
              $.each(data,function(key,value){
                $('#model_attributes').append('<div class="form-check d-inline"><input class="form-check-input" type="checkbox" name="'+table_name+'[]'+'"'+'data-table="'+table_name +'"' +'value="'+table_name+'.'+value+'"'+'><label for="'+value+'" class="form-check-label">'+value+'</label></div>');
              });
            },
            error:function(data){
              alert('there is no table ');
            },
            complete:function(data){
              // $('h4').after().empty();
            }
          });
        }
      },
      error:function(data){
        console.log('error this time');
        $('#related_model1').empty().append('<option value="" selected>Select Relation</option>');
        alert('there is no relation in the selected model');
      },
      complete:function(data){
        console.log('complete');
        // $('h4').remove();
        $('#model_attributes,#related_model1_attributes,#related_model2_attributes,#related_model3_attributes').prev().remove();
        $('#model_attributes,#related_model1_attributes,#related_model2_attributes,#related_model3_attributes').empty();
        // $('#related_model1').empty();

      }
    });
  });
</script>
<script>
  $('#related_model1').on('change',function(){
    var modelName = $(this).val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/attributes',
      type:'POST',
      data:{
        data:modelName
      },
      success:function(data){
        // $('h4:eq(2)').remove();
        $('#related_model1_attributes').empty();
        $('#related_model1_attributes,#related_model2_attributes,#related_model3_attributes').prev().remove();
        $('#related_model1_attributes,#related_model2_attributes,#related_model3_attributes').empty();
        // $('h4').after().empty();
        var table_name = data[data.length-1]; //get table name 
        var attributes = data.pop();   // remove table name
        $('#related_model1_attributes').before('<label>'+table_name+'</label>');
        $.each(data,function(key,value){
          $('#related_model1_attributes').append('<div class="form-check d-inline"><input class="form-check-input" type="checkbox" name="'+table_name+'[]'+'" data-table="'+table_name+'"'+' value="'+table_name+'.'+value+'"'+'><label for="'+value+'" class="form-check-label">'+value+'</label></div>');
        });
        if(data){
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/relations',
            type:'POST',
            data:{
              modelName
            },
            success:function(data){
              $(":input[name='model_relation2']").remove();

              $('#related_model2').empty().append('<option value="" selected>Select Relation</option>');
             $.each(data,function(key,value){
               $('#related_model2').after('<input type="hidden" class="model_relation" name="model_relation2"'+'value="'+key+'"'+'>');
                 $('#related_model2').append('<option value='+value+'>'+key+'</option>');
             });
            },
            error:function(data){
              alert('there is no relation in the selected model');
              $('#related_model2').empty().append('<option value="" selected>Select Relation</option>');
            },
            complete:function(data){
              // $('h4').after().empty();
            }
          });
        }
      },
      error:function(data){
        $('#related_model1_attributes,#related_model2_attributes,#related_model3_attributes').prev().remove();
        $('#related_model1_attributes,#related_model2_attributes,#related_model3_attributes').empty();
        // $('h4:eq(2)').remove();
        alert('there is no table ');
      },
      complete:function(data){
        // $('h4').after().empty();
      }
    });
  });
</script>
<script>
  $('#related_model2').on('change',function(){
    var modelName = $(this).val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/attributes',
      type:'POST',
      data:{
        data:modelName
      },
      success:function(data){
        $('#related_model2_attributes,#related_model3_attributes').prev().remove();
        $('#related_model2_attributes,#related_model3_attributes').empty();
        var table_name = data[data.length-1]; //get table name 
        var attributes = data.pop();   // remove table name
        $('#related_model2_attributes').before('<label>'+table_name+'</label>');
        $.each(data,function(key,value){
          $('#related_model2_attributes').append('<div class="form-check d-inline"><input class="form-check-input" type="checkbox" name="'+table_name+'[]'+'" data-table="'+table_name+'"'+' value="'+table_name+'.'+value+'"'+'><label for="'+value+'" class="form-check-label">'+value+'</label></div>');
        });
        if(data){
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/relations',
            type:'POST',
            data:{
              modelName
            },
            success:function(data){
              $(":input[name='model_relation3']").remove();

              $('#related_model3').empty().append('<option value="" selected>Select Relation</option>');
             $.each(data,function(key,value){
               $('#related_model3').after('<input type="hidden" class="model_relation" name="model_relation3"'+'value="'+key+'"'+'>');
                 $('#related_model3').append('<option value='+value+'>'+key+'</option>');
             });
            },
            error:function(data){
              alert('there is no relation in the selected model');
              $('#related_model3').empty().append('<option value="" selected>Select Relation</option>');
            },
            complete:function(data){
              // $('h4').after().empty();
            }
          });
        }
      },
      error:function(data){
        $('#related_model2_attributes,#related_model3_attributes').prev().remove();
        $('#related_model2_attributes,#related_model3_attributes').empty();
        alert('there is no table ');
      },
      complete:function(data){
        // $('h4').after().empty();
      }
    });
  });
</script>
<script>
  $('#related_model3').on('change',function(){
    var modelName = $(this).val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/attributes',
      type:'POST',
      data:{
        data:modelName
      },
      success:function(data){
        // $('h4:eq(2)').remove();
        $('#related_model3_attributes').empty();
        // $('h4').after().empty();
        var table_name = data[data.length-1]; //get table name 
        var attributes = data.pop();   // remove table name
        $('#related_model3_attributes').before('<label>'+table_name+'</label>');
        $.each(data,function(key,value){
          $('#related_model3_attributes').append('<div class="form-check d-inline"><input class="form-check-input" type="checkbox" name="'+table_name+'[]'+'" data-table="'+table_name+'"'+' value="'+table_name+'.'+value+'"'+'><label for="'+value+'" class="form-check-label">'+value+'</label></div>');
        });
      },
      error:function(data){
        // $('h4:eq(2)').remove();
        $('#related_model3_attributes').empty();
        alert('there is no table ');
      },
      complete:function(data){
        // $('h4').after().empty();
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){

    // add multiple where condition input
    $('.add_multi_where').on('click',function(){
      var base_column = "";
            var inputs = $('#where_condition :input').val();
      console.log(inputs);
      $('.model_attributes :input').each(function(key,value){
        var column =  $(value).val();
        var table = $(value).attr('data-table');

        base_column += '<option data-table="'+table+'"'+'value="'+column+'"'+'>'+column+'</option>';
      });

        // where condition with html 
        var fieldHTML = '<div class="row multi_inputs"><div class="form-group col-4 mt-3"><label>Base Model Columns</label><select class="form-control base_columns field_type" name="where_condition[]" style="width: 100%;"><option selected="selected">Select Base Model</option></select></div><div class="form-group col-4 mt-3"><label>Operations</label><select class="form-control operators" name="where_condition[]" style="width: 100%;"><option selected="selected">Select Operation</option></select></div><div class="form-group col-4 mt-3"><label>Condition Value</label><div class="plus-wrap"><input type="text" class="form-control" name="where_condition[]"><span class="input-group-append plus-inner"><button type="button" class="btn btn-danger btn-flat remove_button">-</button></span></div></div></div>';

      $('#wh_condition').after(fieldHTML); // first where condition 
      // $('.base_columns').empty();
      $('.base_columns').html(base_column); // append selected model attribues in where
      $('.base_columns').append('<option value="" selected>Select Model Column</option>');
      // $(document).closest('.base_columns').html(base_column);
       //Add field html
    });

    // add multiple computed fields condition input
    $('.add_multi_computed').on('click',function(){
      var base_column = "";
      $('.model_attributes :checkbox:checked').each(function(key,value){
        var column =  $(value).val();
        var table = $(value).attr('data-table');

        base_column += '<option data-table="'+table+'"'+'value="'+column+'"'+'>'+column+'</option>';
      });

        // where condition with html 
        var fieldHTML = '<div class="row multi_inputs"><div class="form-group col-3 mt-3"><label>Selected Columns</label><select class="form-control base_computed_columns get_computed_fields" name="computed_fields[]" style="width: 100%;"><option selected="selected">Select Base Model</option></select></div><div class="form-group col-3 mt-3"><label>Computed Functions</label><select class="form-control" name="computed_fields[]" id="computed_fields" style="width: 100%;"><option selected="selected">Select Compute Function</option></select></div><div class="form-group col-3 mt-3"><label>Conditional Value</label><div class="plus-wrap"><input type="text" class="form-control" name="computed_fields[]" value="As" readonly></div></div><div class="form-group col-3 mt-3"><label>New Column Name </label><div class="plus-wrap"><input type="text" class="form-control" name="computed_fields[]"><span class="input-group-append plus-inner"><button type="button" class="btn btn-danger btn-flat remove_button">-</button></span></div></div></div>';

      $('#compute_module').after(fieldHTML); // first where condition 
      // $('.base_columns').empty();
      $('.base_computed_columns').html(base_column); // append selected model attribues in where
      $('.base_computed_columns').append('<option value="" selected>Select Model Column</option>');
      // $(document).closest('.base_columns').html(base_column);
       //Add field html
    });

    $(document).on('click','.remove_button',function() {
        $(this).parents('div.multi_inputs').remove(); //Remove field html
    });

    // Multipe conditions module on card collapse
    $('.various_conditions').on('click',function() {
      $('#where_condition,#compute_selected_columns,#computed_fields,#operators').empty().append('<option value="" selected>Select a Value</option>');
      $('#compute_input2,#wh_condition_input').val('');

      $('#groupby_columns').empty(); // group by condition
      $('.multi_inputs').remove(); //dom added where and compute conditions

      //selected columns from all models in group by conditions and computed field
      $('.model_attributes :checkbox:checked').each(function(key,value){
        var column =  $(value).val();
        var table = $(value).attr('data-table');
        $('#compute_selected_columns').append('<option data-table="'+table+'"'+'value="'+column+'"'+'>'+column+'</option>');
        $('#groupby_columns').append('<option data-table="'+table+'"'+'value="'+column+'"'+'selected>'+column+'</option>');
      });

      //where conditions
      $('.model_attributes :input').each(function(key,value){
        var column =  $(value).val();
        var table = $(value).attr('data-table');
        $('#where_condition').append('<option data-table="'+table+'"'+'value="'+column+'"'+'>'+column+'</option>');
      });
    });

    $('.model_attributes').on('click',':checkbox',function(){
      var cardCheck = $('div').hasClass('collapsed-card');
        if(!cardCheck){
          $('.various_conditions').trigger('click');
        }
    });

  });
</script>
<script>
  $(document).on('change','.field_type',function(){
    var table = $('option:selected',this).attr("data-table");

    var col_val = $(this).val();
    var column = col_val.split(".")[1];
    var element = $(this);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/field_type',
      type:'POST',
      data:{
        table:table,
        column:column
      },
      success:function(data) {
        element.closest('div').next().find('select').empty();
        // element.closest('div').next().next().find('input[type=text]').daterangepicker('destroy');
        if(data==='string' || data==='text'){
          console.log('finish date-range-picker');
          // element.closet('div').next().find('select').empty();
         element.closest('div').next().find('select').append('<option value="=">Equal</option><option value="!=">Not Equal</option>');
        }
        if(data==='bigint' || data==='int' || data==='integer' || data==='float'){
          // element.next().empty();
          element.closest('div').next().find('select').append('<option value="=">Equal</option><option value="!=">Not Equal</option><option value=">">Greater than</option><option value="<">Less than</option><option value=">=">Greater than Equal to</option><option value="<=">Less than Equal to</option>');
        }
        if(data==='date' || data==='datetime'){
          element.closest('div').next().find('select').append('<option value="=">Equal</option><option value="!=">Not Equal</option><option value=">">Greater than</option><option value="<">Less than</option><option value=">=">Greater than Equal to</option><option value="<=">Less than Equal to</option><option value="between">Between</option>');
          // if(data==='date'){
          //   element.closest('div').next().next().find('input[type=text]').daterangepicker({singleDatePicker: true,
          //       showDropdowns: true,
          //       minYear: 1901
          //     });
          // }
          // if(data==='datetime'){
          //   element.closest('div').next().next().find('input[type=text]').datetimepicker();
          // }
        }
      },
      error:function(data){
       alert('somethng went wrong');
      },
    });
  });
</script>
<script>
  $(document).on('change','.get_computed_fields',function(){
    var table = $('option:selected',this).attr("data-table");

    var col_val = $(this).val();
    var column = col_val.split(".")[1];
    var element = $(this);
    console.log(element);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/field_type',
      type:'POST',
      data:{
        table:table,
        column:column
      },
      success:function(data) {
        console.log(data);
        element.closest('div').next().find('select').empty();
        if(data==='bigint' || data==='int' || data==='integer' || data==='float'){
          // element.next().empty();
          element.closest('div').next().find('select').append('<option value="SUM">Sum</option><option value="Count">Count</option>');
        }
      },
      error:function(data){
       alert('somethng went wrong');
      },
    });
  });
</script>
<!-- <script type="text/javascript">
  $('#create_configuration').submit(function(e){
    e.preventDefault();

    var formData = $('#create_configuration').serialize();
    var htmlForm = $(this).clone().html();

    console.log(formData);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url:'/store',
      type:'POST',
      dataType:'html',
      data:{
        html:htmlForm,
        form:formData
      },

      success:function(data) {
        console.log(data);
      },
      // processData: false,
      // contentType: false,
      error:function(data){
       alert('somethng went wrong');
      },
    });
    // var form = $(formData).serialize();
    // console.log(form.html());
  });
</script> -->
<!-- <script type="text/javascript">
    $(document).on('change','.operators',function(){

      var col_val = $(this).val();
      var element = $(this);
      if(col_val ==="between"){
        element.closest('div').next().find('input[type=text]').daterangepicker();
      }
      else{
        console.log('remove daterange');
        element.closest('div').next().find('input[type=text]').data('daterangepicker').container.css('visibility', 'visible');
      }
    });
</script> -->
<script type="text/javascript">
  $('#gen_excel').on('click',function(){
      $("#tblReport").table2excel({
                filename: "Gen_report.xls"
            });
  })
</script>
<!-- <script type="text/javascript">
  function pdfForElement(id) {
    function ParseContainer(cnt, e, p, styles) {
      var elements = [];
      var children = e.childNodes;
      if (children.length != 0) {
        for (var i = 0; i < children.length; i++) p = ParseElement(elements, children[i], p, styles);
      }
      if (elements.length != 0) {
        for (var i = 0; i < elements.length; i++) cnt.push(elements[i]);
      }
      return p;
    }

    function ComputeStyle(o, styles) {
      for (var i = 0; i < styles.length; i++) {
        var st = styles[i].trim().toLowerCase().split(":");
        if (st.length == 2) {
          switch (st[0]) {
            case "font-size":
              {
                o.fontSize = parseInt(st[1]);
                break;
              }
            case "text-align":
              {
                switch (st[1]) {
                  case "right":
                    o.alignment = 'right';
                    break;
                  case "center":
                    o.alignment = 'center';
                    break;
                }
                break;
              }
            case "font-weight":
              {
                switch (st[1]) {
                  case "bold":
                    o.bold = true;
                    break;
                }
                break;
              }
            case "text-decoration":
              {
                switch (st[1]) {
                  case "underline":
                    o.decoration = "underline";
                    break;
                }
                break;
              }
            case "font-style":
              {
                switch (st[1]) {
                  case "italic":
                    o.italics = true;
                    break;
                }
                break;
              }
          }
        }
      }
    }

    function ParseElement(cnt, e, p, styles) {
      if (!styles) styles = [];
      if (e.getAttribute) {
        var nodeStyle = e.getAttribute("style");
        if (nodeStyle) {
          var ns = nodeStyle.split(";");
          for (var k = 0; k < ns.length; k++) styles.push(ns[k]);
        }
      }

      switch (e.nodeName.toLowerCase()) {
        case "#text":
          {
            var t = {
              text: e.textContent.replace(/\n/g, "")
            };
            if (styles) ComputeStyle(t, styles);
            p.text.push(t);
            break;
          }
        case "b":
        case "strong":
          {
            //styles.push("font-weight:bold");
            ParseContainer(cnt, e, p, styles.concat(["font-weight:bold"]));
            break;
          }
        case "u":
          {
            //styles.push("text-decoration:underline");
            ParseContainer(cnt, e, p, styles.concat(["text-decoration:underline"]));
            break;
          }
        case "i":
          {
            //styles.push("font-style:italic");
            ParseContainer(cnt, e, p, styles.concat(["font-style:italic"]));
            //styles.pop();
            break;
            //cnt.push({ text: e.innerText, bold: false });
          }
        case "span":
          {
            ParseContainer(cnt, e, p, styles);
            break;
          }
        case "br":
          {
            p = CreateParagraph();
            cnt.push(p);
            break;
          }
        case "table":
          {
            var t = {
              table: {
                widths: [],
                body: []
              }
            }
            var border = e.getAttribute("border");
            var isBorder = false;
            if (border)
              if (parseInt(border) == 1) isBorder = true;
            if (!isBorder) t.layout = 'noBorders';
            ParseContainer(t.table.body, e, p, styles);

            var widths = e.getAttribute("widths");
            if (!widths) {
              if (t.table.body.length != 0) {
                if (t.table.body[0].length != 0)
                  for (var k = 0; k < t.table.body[0].length; k++) t.table.widths.push("*");
              }
            } else {
              var w = widths.split(",");
              for (var k = 0; k < w.length; k++) t.table.widths.push(w[k]);
            }
            cnt.push(t);
            break;
          }
        case "tbody":
          {
            ParseContainer(cnt, e, p, styles);
            //p = CreateParagraph();
            break;
          }
        case "tr":
          {
            var row = [];
            ParseContainer(row, e, p, styles);
            cnt.push(row);
            break;
          }
        case "td":
          {
            p = CreateParagraph();
            var st = {
              stack: []
            }
            st.stack.push(p);

            var rspan = e.getAttribute("rowspan");
            if (rspan) st.rowSpan = parseInt(rspan);
            var cspan = e.getAttribute("colspan");
            if (cspan) st.colSpan = parseInt(cspan);

            ParseContainer(st.stack, e, p, styles);
            cnt.push(st);
            break;
          }
        case "div":
        case "p":
          {
            p = CreateParagraph();
            var st = {
              stack: []
            }
            st.stack.push(p);
            ComputeStyle(st, styles);
            ParseContainer(st.stack, e, p);

            cnt.push(st);
            break;
          }
        default:
          {
            console.log("Parsing for node " + e.nodeName + " not found");
            break;
          }
      }
      return p;
    }

    function ParseHtml(cnt, htmlText) {
      var html = $(htmlText.replace(/\t/g, "").replace(/\n/g, ""));
      var p = CreateParagraph();
      for (var i = 0; i < html.length; i++) ParseElement(cnt, html.get(i), p);
    }

    function CreateParagraph() {
      var p = {
        text: []
      };
      return p;
    }
    content = [];
    ParseHtml(content, document.getElementById(id).outerHTML);
    return pdfMake.createPdf({
      content: content
    });
  }
</script> -->
<script type="text/javascript">
        $("body").on("click", "#gen_pdf", function () {
            html2canvas(document.getElementById('tblReport'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Gen_report.pdf");
                }
            });
        });
</script>
<script>
  $('#gen_csv').on('click',function(){
    $("#tblReport").table2csv();
  });
</script>
<script>
  @if(session('success'))
    toastr.success("{{ session('success') }}");
  @endif
  @if(session('error'))
    toastr.error("{{ session('error') }}");
  @endif
</script>

