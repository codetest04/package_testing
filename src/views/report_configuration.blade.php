@extends('gen_reports::layouts.app')

@section('header')
<h1>Report Configuration</h1>
@endsection

@section('content')
<form role="form" action="{{ url('/store') }}" method="post" enctype="multipart/form-data" id="create_configuration">
	@csrf
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Create Report Configuration </h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" name="report_name" id="report_name" required>
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" rows="3" placeholder="Enter ..." name="report_description" id="report_description" required></textarea>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Model and Attributes</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Base Model</label>
						<select class="form-control" id="base_model" name="base_model" style="width: 100%;">
							<option value="" selected>Select Base Model</option>
							@foreach($tables as $model => $table)
							   <option value="{{ $model }}">{{ $table }}</option>
							@endforeach							
						</select>
					</div>
					<div class="form-group">
						<label>Related Model1</label>
						<select class="form-control" id="related_model1" name="related_model1" style="width: 100%;">
							<option value="" selected>Select Relation1</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Related Model2</label>
						<select class="form-control" id="related_model2" name="related_model2" style="width: 100%;">
							<option value="" selected>Select Relation2</option>
						</select>
					</div>
					<div class="form-group">
						<label>Related Model3</label>
						<select class="form-control" id="related_model3" name="related_model3" style="width: 100%;">
							<option value="" selected>Select Relation3</option>
						</select>
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					<!-- <label>Base Model Fields</label> -->
					<div class="form-group" id="model_attributes">
						<div class="custom-control custom-checkbox d-inline">
							<!-- <input class="custom-control-input" type="checkbox" value="option1">
							<label for="customCheckbox1" class="custom-control-label">id</label> -->
						</div>
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					<!-- checkbox -->
					<!-- <label>Releted Model1</label> -->
					<div class="form-group" id="related_model1_attributes">
						<div class="custom-control custom-checkbox d-inline">
						<!-- 	<input class="custom-control-input" type="checkbox" value="leave_id">
							<label for="leave_id" class="custom-control-label">leave_id</label> -->
						</div>
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					<!-- checkbox -->
					<!-- <label>Releted Model2</label> -->
					<div class="form-group" id="related_model2_attributes">
						<div class="custom-control custom-checkbox d-inline">
							<!-- <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="leave_id">
							<label for="leave_id" class="custom-control-label">leave_id</label> -->
						</div>
					</div>
				</div>
				<div class="col-sm-3 model_attributes">
					<!-- checkbox -->
					<!-- <label>Releted Model3</label> -->
					<div class="form-group" id="related_model3_attributes">
						<div class="custom-control custom-checkbox d-inline">
							<!-- <input class="custom-control-input" type="checkbox" value="leave_id">
							<label for="leave_id" class="custom-control-label">leave_id</label> -->
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
	<div class="card card-primary collapsed-card">
		<div class="card-header">
			<h3 class="card-title">Conditions</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool various_conditions" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body" style="display: none;">
			<div class="row" id="wh_condition">
				<div class="form-group col-4 mt-3">
					<label>Base Model Columns</label>
					<select class="form-control field_type" name="where_condition[]" id="where_condition" style="width: 100%;">
						<option value="" selected>Select Base Model Column</option>
					</select>
				</div>
				<div class="form-group col-4 mt-3">
					<label>Operations</label>
					<select class="form-control operators" name="where_condition[]" style="width: 100%;" id="operators">
						<option value="" selected>Select Operation</option>
					</select>
				</div>

				<div class="form-group col-4 mt-3">
					<label>Condition Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="where_condition[]" id="wh_condition_input">
						<span class="input-group-append plus-inner">
							<button type="button" class="btn btn-info btn-flat add_multi_where">+</button>
						</span>                           
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 mt-3">
					<label>Group BY</label>
					<select class="form-control select2" id="groupby_columns" name="groupby[]" style="width: 100%;" data-placeholder="select a column" multiple>
					</select>
				</div>
			</div>
			<div class="row" id="compute_module">
				<div class="form-group col-3 mt-3">
					<label>Selected Columns</label>
					<select class="form-control get_computed_fields" name="computed_fields[]" id="compute_selected_columns" style="width: 100%;">
						<option value="" selected>Select Base Model</option>
					</select>
				</div>
				<div class="form-group col-3 mt-3">
					<label>Computed Functions</label>
					<select class="form-control" name="computed_fields[]" id="computed_fields" style="width: 100%;">
						<option value="" selected>Select Compute Function</option>
					</select>
				</div>

				<div class="form-group col-3 mt-3">
					<label>Conditional Value</label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" value="As" readonly>                   
					</div>
				</div>
				<div class="form-group col-3 mt-3">
					<label>New Column Name </label>
					<div class="plus-wrap">
						<input type="text" class="form-control" name="computed_fields[]" id="compute_input2">
						<span class="input-group-append plus-inner">
							<button type="button" class="btn btn-info btn-flat add_multi_computed">+</button>
						</span>      
					</div>
				</div>
				</div>
				<!-- /.col -->

				<!-- /.col -->
			<!-- </div> -->
			<!-- /.row -->
		</div>
		<!-- /.card-body -->
		<div class="card-footer">
			<!-- <input type="submit" class="btn btn-primary" name="preview" value="Preview" id="preview_report"> -->
			<input type="submit" class="btn btn-primary" name="save_configuration" value="Save Configuration" id="save_configuration">
		</div>   
	</div>
</form>
@endsection