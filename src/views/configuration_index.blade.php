@extends('gen_reports::layouts.app')

@section('header')
	<h1>Report Configuration Index</h1>
@endsection

@section('content')
	<div class="card">
        <div class="card-header">
        	<div class="row mb-2">
        		<div class="col-sm-9">
	          		<h3 class="card-title">Report Configuration Page</h3>
	          	</div>
	          	<div class="col-sm-3">
	          		<a href="{{ url('/create') }}">
	          		<button class="btn btn-block btn-primary">Create Configuration</button>
	          		</a>
	      		</div>
          	</div>
        </div>
	            <!-- /.card-header -->
        <div class="card-body">
          	<div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
              	<div class="row">
              		<div class="col-sm-12 col-md-6"></div>
              		<div class="col-sm-12 col-md-6"></div>
              	</div>
              	<div class="row">
              		<div class="col-sm-12">
              			<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
			                <thead>
				                <tr role="row">
				                	<th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Id</th>
				                	<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Name</th>
				                	<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Description</th>
				                	<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Date Created</th>
				                	<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending">Actions</th>
				                </tr>
			                </thead>
	                		<tbody>
	                			@foreach($configurations as $configuration)
				                <tr role="row" class="odd">
				                  <td class="sorting_1">{{ $configuration->id }}</td>
				                  <td>{{ $configuration->name }}</td>
				                  <td>{{ $configuration->description }}</td>
				                  <td>{{ $configuration->created_at }}</td>
				                  <td>
				                  	<a href="{{ url('/report',['id'=>$configuration->id]) }}"><i class="fa fa-eye"></i></a>
				                  	<a href="{{ url('/edit',['id'=>$configuration->id]) }}"><i class="fa fa-edit"></i></a>
				                  	<a href="{{ url('/charts',['id'=>$configuration->id]) }}"><i class="far fa-chart-bar"></i></a>
				                  	  <!-- <i class="fa fa-trash" data-toggle="modal" data-target="#exampleModal"></i> -->
				                  </td>
				                </tr>
				                @endforeach
	            			</tbody>
			                <tfoot>
				                <tr>
				                	<th rowspan="1" colspan="1">Id</th>
				                	<th rowspan="1" colspan="1">Name</th>
				                	<th rowspan="1" colspan="1">Description</th>
				                	<th rowspan="1" colspan="1">Date Created</th>
				                	<th rowspan="1" colspan="1">Actions</th>
				                </tr>
			                </tfoot>
              			</table>
          			</div>	
          		</div>
 			</div>
        </div>
	            <!-- /.card-body -->
  	</div>
@endsection