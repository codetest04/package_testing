<?php

namespace Esfersoft\Gen_Reports;

use Illuminate\Database\Eloquent\Model;

class ReportConfiguration extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description','configuration'
    ];
}
